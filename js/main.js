
//var unique = require('uniq');
/*
Do not Write anything above this line.
Above section will be used only for NPM packpossibleSearchTexts.
*/


var searchURL = 'json/sampleData3.json';
//searchURL = 'https://api.myjson.com/bins/fdyuu';
//searchURL = 'https://api.myjson.com/bins/dlebs';
//searchURL = 'https://api.myjson.com/bins/yhbrs';
var fetchedData_;
var dataList_Complete;
var orderStatus;
var resultList;

var searchingByVoice = false;


var From_date = new Date(moment().subtract(30, 'days'));
var To_date = new Date(moment().startOf('hour'));
var diff_date = To_date - From_date;






$(document).ready(function() {

    $("body").tooltip({ selector: '[data-toggle-tooltip=tooltip]' });
        $('#xml-popup-modal').on('shown.bs.modal', function (e) {
    })

    $('body').on('click', '.show-xml.preview-popup-btn span', function(e) {
    
        var xmlData = $('.xml-preview-small', $(this).closest('td')).val();
        
        $('#xml-popup-modal .xml-preview-large').val(vkbeautify.xml(xmlData));

        $('#xml-popup-modal .xml-preview-large').attr('data-type','xml');
        $(".data-copy-alert").addClass('collapse');

        var forIdData = $('.for-id', $(this).closest('tr')).text();
        $('#xml-popup-modal .modal-title').text(`XML Data for: ${forIdData}`);
        $('#xml-popup-modal .modal-title').attr('rel-xml-order-id',forIdData);
        //console.log('forIdData', forIdData);
        
    })


    $('body').on('click', '.show-error.preview-popup-btn span, .show-comment.preview-popup-btn span', function(e) {
    
        var textData = $('.data-container', $(this).closest('td')).text();
        
        $('#xml-popup-modal .xml-preview-large').val(textData);
        $('#xml-popup-modal .xml-preview-large').attr('data-type','txt');
        $(".data-copy-alert").addClass('collapse');

        var forIdData = $('.for-id', $(this).closest('tr')).text();
        $('#xml-popup-modal .modal-title').text(`Data for: ${forIdData}`);
        $('#xml-popup-modal .modal-title').attr('rel-xml-order-id',forIdData);
        //console.log('forIdData', forIdData);
        
    })
    


    $('body').on('click', '.fs-btn', function(e) {

        $(this).closest('.modal-dialog').toggleClass('fs');

        $('.fa', $(this)).toggleClass('fa-expand');
        $('.fa', $(this)).toggleClass('fa-compress');

        if($(this).closest('.modal-dialog').hasClass('fs')){
            $('.xml-preview-large').animate({'height': (window.innerHeight - 150)},500);
        }else
        {
            $('.xml-preview-large').animate({'height': '150px'},500);
        }
        
    })


    $('body').on('click', '.copy-xml-data-btn', function(e) {
        copyXML();
    })

    $('body').on('click', '.download-xml-data-btn', function(e) {

        var xmpString = $('#xml-popup-modal .xml-preview-large').val();

        var dataTypetoDowanload = $('#xml-popup-modal .xml-preview-large').attr('data-type');

        var downloadableDataFor =  $('#xml-popup-modal .modal-title').attr('rel-xml-order-id');

        if(dataTypetoDowanload == 'xml')
        {
            var blob = new Blob([xmpString], { type: "text/xml" });
        }else
        {
            var blob = new Blob([xmpString], { type: "text/plain" });
        }
        saveAs(blob, `${downloadableDataFor}-${$.now()}.${dataTypetoDowanload}`);
    })

    /* 08-01-2019*/
    $('body').on('click', '.download-exl-data-btn', function(e) {
        var tableID = $(this).closest('.result-table').attr('id');
        fnExcelReport(tableID);
    })

    $('body').on('click', '.print-data-btn', function(e) {
        window.print();
    })

    $('body').on('click', '.pdf-excel-data-btn', function (e) {
        $('body, html').append('<textarea class="convertoStringTextArea" ></textarea>');
        var allOrderIDobj = resultList.resultList[0].orderWiseFootprintList;
        var newCSVList = [];
        $(allOrderIDobj).each(function (index) {
            var this_ = this;
            var index_ = index;
            var newTempList = {};
            for (var key in this_) {
                var simpleStringValue = "" + this_[key];
                if (simpleStringValue != 'null' || simpleStringValue != "" || simpleStringValue != "undefined") {
                    if (key == 'error' || key == 'serviceName' || key == 'request' || key == 'response' || key == 'comment' || key == 'createdOn' || key=='status') { 
                    //if (key == 'error' || key == 'serviceName' || key == 'request' || key == 'response' || key == 'comment' || key == 'status' || key == 'createdon') {
                        simpleStringValue = simpleStringValue.replace(/[\r\n\t]/g, " ");
                        simpleStringValue = encodeURI(simpleStringValue);
                        $('.convertoStringTextArea').val(simpleStringValue);
                        newTempList['' + key + ''] = decodeURI(String($('.convertoStringTextArea').val()));
                    }
                }
            }
            newCSVList[index_] = newTempList;
        })


       // newCSVList.splice(0, 7, 'serviceName', 'request', 'response', 'createdOn', 'status', 'error', 'comment');


        var FileNameforCSV = resultList.customerId + '_' + $.now();
        var SheetTitle = 'Report for: ' + resultList.customerId;
        JSONToCSVConvertor(newCSVList, SheetTitle, 'Yes', FileNameforCSV);
        $('.convertoStringTextArea').remove();
    })


    $('body').on('change', '#ofStatus', function(e) {
        orderStatus = $(this).val();
    })


    $('body').on('keypress', '#fetch-customer-data-form .customer-input-field', function(e) {
        
        var t = $(this);
        var tClass = $(this).attr('class');
        $('#fetch-customer-data-form .customer-input-field').each(function(){
            var thisClassName = $(this).attr('class');
            if(tClass != thisClassName)
            {
                $(this).val('');
            }
       })
    })



    $('body').on('click', '#fetch-customer-data-form .get-customer-data', function(e) {
        e.preventDefault();

        if($('.order-id').val() == "" && $('.quotes-id').val() == "" && $('.customer-id').val() == "" )
        {
            $('.form-error_').removeClass('collapse');
        }else
        {
            $('.form-error_').addClass('collapse');

            callAjaxHBS();
        }

        
       
    });


    $('body').on('click', '.last-sync .fa-refresh', function(e) {

        
        resetSyncMsg();

        var t = $(this);
        $('.event-loader').removeClass('hidden');
        $(t).addClass('fa-rotation');
        
        var lastSyncAjaxURL = "http://localhost:10033/";
        
        $.ajax({
            method: "GET",
            url: lastSyncAjaxURL,
            success: function(data) {

                var currentdate = new Date(); 
                var datetime = currentdate.getDate() + "."
                + (currentdate.getMonth()+1)  + "." 
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes();

                // @Amar - Please replace this by your value

                //datetime = yourVal



                $('.last-sync-text span').text( datetime);
                $('.event-loader').addClass('hidden');
                $(t).removeClass('fa-rotation');

                $('.last-sync').addClass('sync-success');

                $('.last-sync-text-alert').text('Sync Success !!!');
                setTimeout(function(){
                    resetSyncMsg();
                },15000)

            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log('Try again later.');
                $('.event-loader').addClass('hidden');
                $(t).removeClass('fa-rotation');

                $('.last-sync').addClass('sync-error');
                $('.last-sync-text-alert').text('Sync error, Try again.');
                setTimeout(function(){
                    resetSyncMsg();
                },15000)
            },
            fail: function (xhr) {
                    console.log('Try again later.');
                    $('.event-loader').addClass('hidden');
                    $(t).removeClass('fa-rotation');
                    $('.last-sync').addClass('sync-error');
                    $('.last-sync-text-alert').text('Sync error, Try again.');
                    setTimeout(function(){
                        resetSyncMsg();
                    },15000)
            }
        });



    });


    $('body').on('click', '.last-sync .last-sync-toggle', function(e) {
       
        var tickerWidth = $('.last-sync').outerWidth();
        var t = $(this);
        if($('.last-sync').hasClass('small-i')){

         $('.last-sync').removeClass('small-i');

         
         $('.last-sync-toggle .fa-chevron-left').removeClass('fa-chevron-left').addClass('fa-chevron-right');


          $('.last-sync').css({
               'right': 0
           })
        }else{
            $('.last-sync').addClass('small-i');
            $('.last-sync-toggle .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-left');

           $('.last-sync').css({
               'right': - (tickerWidth - 38)
            })
        }



    });
 

    $('body').on('click', '.scroll-to-top', function(e) {
       

        $('html, body').animate({
            scrollTop: 0
        }, 800, function() {
            
        }); 

    });

    $('body').on('click', '.theme-toggle input[type="checkbox"]', function(e) {
   
        if($(this).prop("checked") == true){ 
            $("#parker-theme-css").removeAttr("disabled");
            $('.theme-toggle').attr('data-original-title','Activate Bootstrap Theme');
        }
        else if($(this).prop("checked") == false){
            
            $("#parker-theme-css").attr("disabled", "disabled");
            $("#parker-theme-css").attr("disabled", "disabled");
            $('.theme-toggle').attr('data-original-title','Activate Parker Theme');
        }
    });

   
 
    $('input[name="daterange"]').daterangepicker({
        "showDropdowns": true,
        "timePicker24Hour": true,
        "autoApply": true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
       
        "alwaysShowCalendars": true,
        "opens": "left",
        "drops": "up",
        timePicker: true,
        startDate: moment().subtract(30, 'days'),
        endDate: moment().startOf('hour'),
        locale: {
          format: 'MM/DD/YY hh:mm AA'
        }
    });
    
    

    /* End of doc reaDY */
});




$('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
    
    From_date = new Date(picker.startDate.format('YYYY-MM-DD'));
    To_date = new Date(picker.endDate.format('YYYY-MM-DD'));
    diff_date = To_date - From_date;

    var years = Math.floor(diff_date / 31536000000);
    var months = Math.floor((diff_date % 31536000000) / 2628000000);
    var days = Math.floor(((diff_date % 31536000000) % 2628000000) / 86400000);
    //$("#Result").html(years + " year(s) " + months + " month(s) " + days + " and day(s)");
    console.log( years+" year(s) "+months+" month(s) "+days+" and day(s)");
    console.log(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    //$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    //$(this).val(years+" year(s) "+months+" month(s) and "+days+" day(s)");

});

$('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
});



$(window).scroll(function(e){
    var scrollTopVal = $(this).scrollTop();

    var tickerWidth = $('.last-sync').outerWidth();

    if(scrollTopVal> 300)
    {
        $('.last-sync').addClass('small-i');
        $('.last-sync').css({
            'right': - (tickerWidth - 38)
        })

        $('.last-sync-toggle .fa-chevron-right').removeClass('fa-chevron-right').addClass('fa-chevron-left');

        // scroll-to-top
        $('.scroll-to-top').removeClass('hidden');
        
    }else
    {
        $('.last-sync').removeClass('small-i');
        $('.last-sync').css({
            'right': 0
        })
       
        $('.last-sync-toggle .fa-chevron-left').removeClass('fa-chevron-left').addClass('fa-chevron-right');
        // scroll-to-top
        $('.scroll-to-top').addClass('hidden');
    }
    
});

function copyXML() {

    $('#xml-popup-modal .xml-preview-large').removeAttr('disabled');

    var copyText = $('#xml-popup-modal .xml-preview-large');
    copyText.select();
    
    document.execCommand("copy");
    $(".data-copy-alert").removeClass('collapse');

    $('#xml-popup-modal .xml-preview-large').attr('disabled', 'disabled');

}

function callAjaxHBS(){
    $('.event-loader').removeClass('hidden');
    var orderId = $('.order-id').val();
    var quotesId = $('.quotes-id').val();
    var customerId = $('.customer-id').val();
    
    $.ajax({
        method: "GET",
        url: searchURL,
        cache: false,
        data: 
        {
            'orderId': orderId,
            'quotesId': quotesId,
            'customerId': customerId
        },
        success: function(data) {
            resultList = data;
             
            // var allOrderIDobj = resultList.resultList[0].CustomerOrderData.OrderData;
            // var allOrderID = [];
            // for (var id in allOrderIDobj) {
            //     allOrderID.push(allOrderIDobj[id].orderId);
            // }
            // getUniqueOrderID = [...new Set(allOrderID)];
            loadHBSforCustomerData();
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('Try again later.');
            $('.event-loader').addClass('hidden');
       },
       fail: function (xhr) {
            console.log('Try again later.');
            $('.event-loader').addClass('hidden');
       }
    });
}


function loadHBSforCustomerData() {
    loadCustomerData(function (template) {
        	
        var partialData = {
            "List": resultList
        };
        // console.log(partialData.resultList);
        var content = template(partialData);
        $('.fetched-customer-data').html(content);
        
        $('.fetched-customer-order-data').removeClass('hidden');
        $('.fetched-customer-data').removeClass('hidden');
        $('.event-loader').addClass('hidden');
        $('html, body').animate({
            scrollTop: $(".fetched-customer-data").offset().top
        }, 1000, function() {
            $('.event-loader').removeClass('hidden');
            loadHBSforCustomerOrderData();
        });       
        
    });

}

function resetSyncMsg(){
        $('.last-sync').removeClass('sync-error');
        $('.last-sync').removeClass('sync-success');
        
        $('.last-sync-text-alert').text('');
}

function resetAllContainer(){
    $('.fetched-customer-order-data').html('');
    $('.fetched-customer-order-data').addClass('hidden');

    $('.fetched-customer-data').html('');
    $('.fetched-customer-data').addClass('hidden');

}

function loadHBSforCustomerOrderData() {
    loadCustomerOrderData(function (template) {
        	
        var partialData = {
            "List": resultList
        };
        // console.log(partialData.resultList);
        var content = template(partialData);
        $('.fetched-customer-order-data').html(content);
        
        $('.fetched-customer-order-data').removeClass('hidden');
       
        $('.event-loader').addClass('hidden');
        $('html, body').animate({
            scrollTop: $(".fetched-customer-order-data").offset().top
        }, 1000, function() {
            setOrderStatusFilterValueSelected();
        });


       
       
       
    });

}

let setOrderStatusFilterValueSelected = () => {
    var orderStatus =  $('#ofStatus').val();
    $('.fetched-filter-status').text(orderStatus);
    $('.fetched-date-range-selected').text(`${From_date.toISOString().slice(0,10)} to ${To_date.toISOString().slice(0,10)}`);
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel, fileName) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
   // CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var fileName = fileName;
    //this will remove the blank-spaces from the title and replace it with an underscore
    //fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}








/*-----------------------------
      Voice Recognition App
------------------------------*/



try {
    var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
    var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;


    var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral', 'crimson', 'cyan', 'fuchsia', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'indigo', 'ivory', 'khaki', 'lavender', 'lime', 'linen', 'magenta', 'maroon', 'moccasin', 'navy', 'olive', 'orange', 'orchid', 'peru', 'pink', 'plum', 'purple', 'red', 'salmon', 'sienna', 'silver', 'snow', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'white', 'yellow'];
    var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;'

    
    
    var recognition = new SpeechRecognition();
    var speechRecognitionList = new SpeechGrammarList();
    speechRecognitionList.addFromString(grammar, 1);
    recognition.grammars = speechRecognitionList;
    
    //recognition.lang = 'en-US';
    //recognition.interimResults = false;
    //recognition.maxAlternatives = 1;
    $('body').addClass("voice-activated");
}
catch (e) {
    console.error(e);
}



var noteTextarea = $('#note-textarea');
var instructions = $('#recording-instructions');
var notesList = $('ul#notes');

var noteContent = '';

var recording = false;


/*-----------------------------
      Voice Recognition 
------------------------------*/

// If false, the recording will stop after a few seconds of silence.
// When true, the silence period is longer (about 15 seconds),
// allowing us to keep recording even when the user pauses. 
recognition.continuous = false;

// This block is called every time the Speech APi captures a line. 
recognition.onresult = function(event) {

  // event is a SpeechRecognitionEvent object.
  // It holds all the lines we have captured so far. 
  // We only need the current one.
  var current = event.resultIndex;

  // Get a transcript of what was said.
  var transcript = event.results[current][0].transcript;

  // Add the current transcript to the contents of our Note.
  // There is a weird bug on mobile, where everything is repeated twice.
  // There is no official solution so far so we have to handle an edge case.
  var mobileRepeatBug = (current == 1 && transcript == event.results[0][0].transcript);

  if(!mobileRepeatBug) {
    //noteContent += transcript;
    noteContent = transcript;
    noteTextarea.html(noteContent);
    console.log('Confidence: ' + event.results[0][0].confidence);

    var searchStr = noteContent.toLowerCase();
    //console.log(searchStr);

    setandSearhbyText(searchStr);
    
    


  }
  $('#note-textarea').show();
  $('.main-voice-input').removeClass('listening');
};

recognition.onnomatch = function(event) {
    //instructions.text("I didn't recognise that color.");
    console.log("I didn't recognise that color.");
}

// recognition.onmatch = function(event) {
//     //instructions.text("I didn't recognise that color.");
//     console.log("Got that color.");
// }
  


/*On Start*/
recognition.onstart = function() { 
  instructions.text('Voice recognition activated. Try speaking into the microphone.');
  $('.main-voice-input').addClass('listening-ready');
}
recognition.onaudiostart = function () {
    //Fired when the user agent has started to capture audio.
    console.log('onaudiostart');
    $('.main-voice-input').addClass('listening');
}


recognition.onsoundstart = function () {
    //Fired when any sound — recognisable speech or not — has been detected.
    console.log('onsoundstart');
    $('.main-voice-input').addClass('listening');
}


/*On End*/


recognition.onaudioend = function () {
    //Fired when the user agent has finished capturing audio.
    console.log('onaudioend');
    recording = false;
}
recognition.onend = function () {
    //Fired when the speech recognition service has disconnected.
    console.log('onend');
    recording = false;
}

recognition.onsoundend = function () {
    //Fired when any sound — recognisable speech or not — has stopped being detected.
    console.log('onsoundend');
    instructions.text('Voice recognition paused.');
    $('.main-voice-input').removeClass('listening listening-ready');
    recording = false;
}

recognition.onspeechend = function() {
    recognition.stop();
    instructions.text('You were quiet for a while so voice recognition turned itself off.');
    recording = false;
    $('.main-voice-input').removeClass('listening listening-ready');
}



/*On Error*/
recognition.onerror = function(event) {
    console.log(event.error);
    
  if(event.error == 'no-speech') {
    instructions.text('No speech was detected. Try again.');  
  };
}





/*-----------------------------
      App buttons and input 
------------------------------*/

$('#start-record-btn').on('click', function(e) {

    searchingByVoice = true;

    if(!recording)
    {
        recording = true;
        if (noteContent.length) 
        {
            noteContent += ' ';
        }
        recognition.start();
        instructions.text('Listening.');
        
    }else
    {
        recognition.stop();
        instructions.text('Voice recognition paused.');
        recording = false;
    }
    
  });
  
  
  
  
  /*-----------------------------
        Speech Synthesis 
  ------------------------------*/
  
  function readOutLoud(message) {
    if(searchingByVoice)
    {
      var speech = new SpeechSynthesisUtterance();
  
      // Set the text and voice attributes.
        speech.text = message;
        speech.volume = 1;
        speech.rate = 1;
        speech.pitch = 1;
      
        window.speechSynthesis.speak(speech);
    }
    
  }
  


  



function setandSearhbyText(voiceSearchedString) {
    console.log('Searching in array');
    
    var possibleSearchTexts = ['OrderID', 'Order ID', 'CustomerID', 'Customer ID', 'QuotesID', 'Quotes ID', 'QuoteID', 'Quote ID', 'Voter ID'];
    //var voiceSearchedString = "Welcome to CustomerID."; 
    
    var valueFound = "";
    var indextoStart = 0;
    var searchedFor = "";

    possibleSearchTexts.forEach(function(value, index, array){
        //console.log(value, index);
        var check = voiceSearchedString.includes(value.toLowerCase()); 
       

        if(!check)
        {
           // console.log(voiceSearchedString + ' not found in this index '+ index);

           //(`No exact word found to perform search`);
        //    instructions.text(`No exact word found to perform search, 
        //    Click on mic to try again or try enetering value manually.`);


        }else
        {
            //console.log(value +' Found in voice text ==> '+ voiceSearchedString + ' at this index '+ index);
            valueFound = value.toLowerCase();

            instructions.text(``);
        

            
        }
        
      });

        if(valueFound != ""){
            //console.clear();
           // voiceSearchedString  = 'Sexcx arch my Customer id 124 4545 4545';
            voiceSearchedString = voiceSearchedString.toLowerCase();
            /*AI Algo*/
           

            
            // get last all digit starting from last index of text removed
            var actualSearchText = voiceSearchedString.split(valueFound);
            // Remove all space from last refined value
            if(actualSearchText){
               // actualSearchText = " "+actualSearchText;
               //   actualSearchText = actualSearchText[1].replace(/\s/g,'')
               //actualSearchText = $.trim(actualSearchText);
               console.log(`Actual Search Text ==>  ${actualSearchText[1]} --- ${valueFound}`);
            }
            
            

            if(valueFound == 'orderid' || valueFound == 'order id' || valueFound == 'voterid' || valueFound == 'voter id'  )
            {
                $('.order-id').val(actualSearchText[1]);
                searchedFor = "Order ID ";
            }else
            {
                $('.customer-id').val(actualSearchText[1]);
                searchedFor = "Customer ID ";
            }
           
            readOutLoud(`Searching for, ${searchedFor} ${actualSearchText[1]}`);

            setTimeout(function(){
                $('#voice-popup-modal').modal('hide');
            }, 3000);   

            setTimeout(function(){
                $('.get-customer-data').trigger('click');
            }, 4500);

            setTimeout(function(){
                readOutLoud('Here is the results');
            }, 6000);


        }


}









// var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
// var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
// var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

// var phrases = [
//   'Order ID',
//   'Customer ID',
//   'Quotes ID',
//   'Quote ID'
  
// ];

// var phrasePara = document.querySelector('.phrase');
// var resultPara = document.querySelector('.result');
// var diagnosticPara = document.querySelector('.output');

// var noteTextarea = $('#note-textarea');
// var instructions = $('#recording-instructions');


// var noteContent = '';

// var recording = false;



// var testBtn = document.querySelector('button.tst');

// function randomPhrase() {
//   var number = Math.floor(Math.random() * phrases.length);
//   return number;
// }

// function testSpeech() {
//   testBtn.disabled = true;
//   testBtn.textContent = 'Test in progress';

//   var phrase = phrases[randomPhrase()];
//   // To ensure case consistency while checking with the returned output text
//   phrase = phrase.toLowerCase();
//   phrasePara.textContent = phrase;
//   resultPara.textContent = 'Right or wrong?';
//   resultPara.style.background = 'rgba(0,0,0,0.2)';
//   diagnosticPara.textContent = '...diagnostic messpossibleSearchTexts';

//   var grammar = '#JSGF V1.0; grammar phrase; public <phrase> = ' + phrase +';';
//   var recognition = new SpeechRecognition();
//   var speechRecognitionList = new SpeechGrammarList();
//   speechRecognitionList.addFromString(grammar, 1);
//   recognition.grammars = speechRecognitionList;
//   recognition.lang = 'en-US';
//   recognition.interimResults = false;
//   recognition.maxAlternatives = 1;

//   recognition.start();

//   recognition.onresult = function(event) {
//     // The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
//     // The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
//     // It has a getter so it can be accessed like an array
//     // The first [0] returns the SpeechRecognitionResult at position 0.
//     // Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
//     // These also have getters so they can be accessed like arrays.
//     // The second [0] returns the SpeechRecognitionAlternative at position 0.
//     // We then return the transcript property of the SpeechRecognitionAlternative object 
//     var speechResult = event.results[0][0].transcript.toLowerCase();
//     diagnosticPara.textContent = 'Speech received: ' + speechResult + '.';
//     //if(speechResult === phrase) {


//         var array = [
//             'Order ID',
//             'Customer ID',
//             'Quotes ID',
//             'Quote ID'
            
//           ];
          

//         var el = array.find(a =>a.includes('ID'));
        
//         console.log(el)


//     if(speechResult.includes(phrase) ){
//       resultPara.textContent = 'I heard the correct phrase!';
//       resultPara.style.background = 'lime';
//     } else {
//       resultPara.textContent = 'That didn\'t sound right.';
//       resultPara.style.background = 'red';
//     }

//     console.log('Confidence: ' + event.results[0][0].confidence);
//   }

//   recognition.onspeechend = function() {
//     recognition.stop();
//     testBtn.disabled = false;
//     testBtn.textContent = 'Start new test';
//   }

//   recognition.onerror = function(event) {
//     testBtn.disabled = false;
//     testBtn.textContent = 'Start new test';
//     diagnosticPara.textContent = 'Error occurred in recognition: ' + event.error;
//   }
  
//   recognition.onaudiostart = function(event) {
//       //Fired when the user agent has started to capture audio.
//       console.log('SpeechRecognition.onaudiostart');
//   }
  
//   recognition.onaudioend = function(event) {
//       //Fired when the user agent has finished capturing audio.
//       console.log('SpeechRecognition.onaudioend');
//   }
  
//   recognition.onend = function(event) {
//       //Fired when the speech recognition service has disconnected.
//       console.log('SpeechRecognition.onend');
//   }
  
//   recognition.onnomatch = function(event) {
//       //Fired when the speech recognition service returns a final result with no significant recognition. This may involve some degree of recognition, which doesn't meet or exceed the confidence threshold.
//       console.log('SpeechRecognition.onnomatch');
//   }
  
//   recognition.onsoundstart = function(event) {
//       //Fired when any sound — recognisable speech or not — has been detected.
//       console.log('SpeechRecognition.onsoundstart');
//   }
  
//   recognition.onsoundend = function(event) {
//       //Fired when any sound — recognisable speech or not — has stopped being detected.
//       console.log('SpeechRecognition.onsoundend');
//   }
  
//   recognition.onspeechstart = function (event) {
//       //Fired when sound that is recognised by the speech recognition service as speech has been detected.
//       console.log('SpeechRecognition.onspeechstart');
//   }
//   recognition.onstart = function(event) {
//       //Fired when the speech recognition service has begun listening to incoming audio with intent to recognize grammars associated with the current SpeechRecognition.
//       console.log('SpeechRecognition.onstart');
//   }
// }
// $('body').addClass("voice-activated");
// testBtn.addEventListener('click', testSpeech);



/* <p class="rm">The idea is to minimize the effort if looking out for some particular orders which happen to
 fail based on some unavoidable factors due to some service malfunction. <a href="#">read more...</a> </p>*/

// $('.rm').each(function(){
//     var completeTagText = $(this).html();
//     var tagDevider = '<a';
//     var devidedTexts = completeTagText.split(tagDevider);
//     var firstPart = devidedTexts[0];
//     firstPart = firstPart.substring(0,50);  // starting 0 upto 50 char // will show only first 50 char
//     var secondPart = devidedTexts[1];
//     secondPart = tagDevider +' '+ secondPart;
//     $(this).html(firstPart + secondPart);
// });

