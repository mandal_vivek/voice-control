var masterTpl;

// function defination
//console.log('search-top-helper.js');

function loadCustomerData(done){
    //console.log('search-top-helper.js');

    $.ajax({
      method : 'GET',
      url : './partials/fetch-customer-data.html',
      cache: false,
      success : function(data){
        
        var tmp = Handlebars.compile(data);
        Handlebars.registerPartial('masterTpl', tmp);
        masterTpl = tmp;
        done(tmp);
       
      },
      fail : function(xhr) {
        console.log(xhr);
      }
    });
}

